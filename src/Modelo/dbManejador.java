/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.sql.*;


/**
 *
 * @author angel
 */
public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario, contraseña, baseDatos, driver, url;

    public dbManejador() {
        this.baseDatos = "Prueba";
        this.usuario = "angel";
        this.contraseña = "123";
        this.driver = "com.mysql.cj.jdbc.Driver";
        this.url = "jdbc:mysql://18.222.80.97:3306/" + this.baseDatos;
        this.EsDrive();
    }

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String contraseña, String baseDatos, String driver, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.baseDatos = baseDatos;
        this.driver = driver;
        this.url = url;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean EsDrive(){
        boolean exito = false;
        try {
            Class.forName(driver);
            exito = true;
        } catch (ClassNotFoundException e) {
            System.out.println("Surgio un error " + e.getMessage());
            System.exit(-1);
        }
    return exito;
}
    
    public boolean conectar(){
        boolean exito = false;
        try {
            this.setConexion(DriverManager.getConnection(this.url, this.usuario, this.contraseña));
            exito = true;
        } catch (SQLException e){
            System.out.println("Surgio un error " + e.getMessage());
        }
        return exito;
    }
    
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()) this.conexion.close();
        } catch(SQLException e) {
            System.out.println("Surgio un error al cerrar la conexion: " + e.getMessage());
        }
    }
    
}
